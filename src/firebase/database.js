import firebase from 'firebase/compat/app';
import 'firebase/compat/database';
import {getDatabase, ref, onValue, push, set, update, get, } from "firebase/database";

const firebaseConfig = {
  apiKey: "AIzaSyCAIJdN3PwoIL96KaiMBCXBE7VsPaqMZcA",
  authDomain: "planning-poker-39db8.firebaseapp.com",
  databaseURL: "https://planning-poker-39db8-default-rtdb.firebaseio.com",
  projectId: "planning-poker-39db8",
  storageBucket: "planning-poker-39db8.appspot.com",
  messagingSenderId: "195253042693",
  appId: "1:195253042693:web:abd390bc8150312853666c"
};
  firebase.initializeApp(firebaseConfig);

  const database = firebase.database();
  
  const roomsRef = database.ref('rooms');
  
  export { database, roomsRef, onValue, push, set, update, get };