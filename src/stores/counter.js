import { defineStore } from 'pinia';
import { ref, computed, onUnmounted, reactive } from 'vue';
import { database } from '../firebase/database';
import { useRouter } from 'vue-router';
import Cookies from 'js-cookie';

export const useCounterStore = defineStore('counter', () => {
  const playerName = ref('');
  const roomRef = database.ref('rooms');
  const roomId = ref('');
  const playersData = ref([]);
  const isRoomMaster = ref(false);
  const playerId = ref('');


  const state = reactive({
    roomId: roomId,
  });

  const updatePlayerCards = (snapshot) => {
    if (snapshot && snapshot.val) {
      const rooms = snapshot.val();
      if (typeof rooms === 'object' && rooms !== null) {
        playersData.value = Object.keys(rooms).map((key) => ({
          ...rooms[key],
          roomId: key,
        }));
      }
    }
  };
  
  

  roomRef.on('value', updatePlayerCards);

  onUnmounted(() => {
    roomRef.off('value', updatePlayerCards);
  });

  async function createRoom(roomName, playerName) {
    try {
      const roomData = {
        roomName: roomName,
        players: [{ role: 'roomMaster', name: playerName, score: null }],
      };
      const roomSnapshot = await roomRef.push(roomData);
      const roomId = roomSnapshot.key;
      console.log('Room created with ID:', roomId);
  
      localStorage.setItem('playerRole', 'roomMaster');
  
      roomRef
        .child(roomId)
        .child('players')
        .on('value', updatePlayerCards);
  
      setJoinedStatus();
  
      return roomId;
    } catch (error) {
      console.error('Error creating room:', error);
      throw error;
    }
  }
  
  
  function setJoinedStatus() {
    localStorage.setItem('joinedStatus', 'true');
  }

  async function checkPlayerRole() {
    const currentUserName = await getPlayerName();
    const currentRoomId = router.currentRoute.value.params.roomId;
    const roomSnapshot = await getRoomDoc(currentRoomId);
  
    // Check if the player is the room master based on local storage
    const isRoomMasterLocal = localStorage.getItem('isRoomMaster') === 'true';
  
    if (roomSnapshot.exists()) {
      const roomData = roomSnapshot.val();
      const players = Array.isArray(roomData.players) ? roomData.players : [];
  
      const currentPlayer = players.find((player) => player.name === currentUserName);
  
      if (currentPlayer) {
        if (isRoomMasterLocal && currentPlayer.role === 'roomMaster') {
          isRoomMaster.value = true;
        } else {
          isRoomMaster.value = false;
        }
        state.roomId = currentRoomId;
      }
    } else {
      isRoomMaster.value = false;
      state.roomId = '';
    }
  }
  
  

  async function joinRoom(roomId, playerName) {
    try {
      const roomSnapshot = await roomRef.child(roomId).get();
      if (roomSnapshot.exists()) {
        const roomData = roomSnapshot.val();
        const players = Array.isArray(roomData.players) ? roomData.players : [];

        const existingPlayer = players.find((player) => player.name === playerName);

        if (existingPlayer) {
          console.log('Player already exists in the room');
        } else {
          const newPlayer = { name: playerName, score: null };
          await roomRef.child(roomId).child('players').push(newPlayer);
          console.log('Joined room successfully');

          roomRef
            .child(roomId)
            .child('players')
            .on('value', updatePlayerCards);
        }
      } else {
        console.log('Room does not exist');
      }
    } catch (error) {
      console.error('Error joining room:', error);
    }
  }

  async function getPlayerName() {
    return playerName.value;
  }

  async function getRoomDoc(roomId) {
    try {
      return await roomRef.child(roomId).once('value');
    } catch (error) {
      console.error('Error getting room document:', error);
      throw error;
    }
  }

  async function deleteRoom(roomId) {
    try {
      await roomRef.child(roomId).remove();
      console.log('Room deleted successfully');
    } catch (error) {
      console.error('Error deleting room:', error);
    }
  }


  async function leaveRoom(playerId) {
    try {
      console.log('Current user:', playerName.value);
  
      if (!roomId.value || roomId.value === '') {
        console.error('Invalid room ID');
        return;
      }
  
      roomRef.child(roomId.value).child('players').off('value', updatePlayerCards);
  
      const roomSnapshot = await roomRef.child(roomId.value).get();
      if (roomSnapshot.exists()) {
        console.log('Room exists');
        const roomData = roomSnapshot.val();
        const players = Array.isArray(roomData.players) ? roomData.players : [];
  
        console.log('Current players:', players);
  
        const updatedPlayers = players.filter((player) => player.playerId !== playerId);
        await roomRef.child(roomId.value).child('players').set(updatedPlayers);
        console.log('Left the room successfully');
      } else {
        console.log('Room does not exist');
      }
  
      clearJoinedStatus();
  
      Cookies.remove('isRoomMaster');
    } catch (error) {
      console.error('Error leaving the room:', error);
    }
  }
  

function clearJoinedStatus(){
  localStorage.removeItem('joinedStatus')
}
  

  async function checkRoomExists(roomId) {
    try {
      const roomSnapshot = await roomRef.child(roomId).get();
      return roomSnapshot.exists();
    } catch (error) {
      console.error('Error checking room existence:', error);
      return false;
    }
  }

  async function getRoomIdsFromFirebase() {
    try {
      const snapshot = await roomRef.get();
      const roomIds = [];
      snapshot.forEach((childSnapshot) => {
        roomIds.push(childSnapshot.key);
      });
      return roomIds;
    } catch (error) {
      console.error('Error fetching room IDs from Firebase:', error);
      throw error;
    }
  }

  async function getRoomData(roomId) {
    try {
      const roomSnapshot = await roomRef.child(roomId).get();
      return roomSnapshot.val();
    } catch (error) {
      console.error('Error fetching room data:', error);
      return null;
    }
  }

  async function checkPlayerExists(roomId, playerName) {
    const roomSnapshot = await roomRef.child(roomId).get();
    if (roomSnapshot.exists()) {
      const roomData = roomSnapshot.val();
      const players = Array.isArray(roomData.players) ? roomData.players : [];
      const existingPlayer = players.find((player) => player.name === playerName);
      return !!existingPlayer;
    } else {
      console.log('Room does not exist');
      return false;
    }
  }
  function getPlayerId() {
    return playerId.value;
  }
  
  const removePlayerFromRoom = async (roomId, playerId, currentPlayerId) => {
    try {
      const roomRef = database.ref(`/rooms/${roomId}/players/${playerId}`);
      const playerRef = database.ref(`/players/${playerId}`);
  
      await roomRef.remove();
      console.log('Player removed successfully from the room');
  
      // Periksa apakah pemain yang dikeluarkan adalah pemain saat ini yang sedang menggunakan aplikasi
      if (currentPlayerId === playerId) {
        await playerRef.remove();
        console.log('Player removed successfully from the players collection');
      }
    } catch (error) {
      console.error('Error removing player:', error);
    }
  };
  
  
  
  
  
  
  
  
  
  
  
  

  return {
    playerName,
    roomId,
    playersData,
    isRoomMaster,
    state,
    playerId,
    roomRef,
    getPlayerId,
    createRoom,
    setJoinedStatus,
    checkPlayerRole,
    joinRoom,
    getPlayerName,
    getRoomDoc,
    deleteRoom,
    leaveRoom,
    checkRoomExists,
    getRoomIdsFromFirebase,
    getRoomData,
    checkPlayerExists,
    removePlayerFromRoom,
  };
});
